package net.softempresarial.inboxsim.servlets;

import java.io.File;
import java.util.Arrays;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.subethamail.smtp.server.SMTPServer;

import net.softempresarial.inboxsim.mail.InboxSimAuthHandlerFactory;
import net.softempresarial.inboxsim.mail.InboxSimMessageHandlerFactory;
import net.softempresarial.inboxsim.util.ConfigReader;
import net.softempresarial.inboxsim.util.HTMLFilenameFilter;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class StartupServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);

		try {
			ConfigReader config = new ConfigReader(getServletContext());

			File fOutputDir = new File(config.getOutputDir());
			File[] allFiles = fOutputDir.listFiles(new HTMLFilenameFilter());
			Arrays.sort(allFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);

			int counter = 0;
			for(File file : allFiles) {
				counter++;
				if(counter >= 2000) {
					if(!file.isDirectory()) {
						file.delete();
					} else {
						FileUtils.deleteDirectory(file);
					}
				}
			}

			InboxSimMessageHandlerFactory handlerFactory = new InboxSimMessageHandlerFactory();
			handlerFactory.setConfig(config);
			SMTPServer smtpServer = new SMTPServer(handlerFactory);
			InboxSimAuthHandlerFactory authHandlerFactory = new InboxSimAuthHandlerFactory();
			authHandlerFactory.setConfig(config);
			smtpServer.setAuthenticationHandlerFactory(authHandlerFactory);
			smtpServer.setPort(config.getPort());
			smtpServer.setEnableTLS(config.isEnableTLS());
			
			// TODO: Agregar parámetros a la configuración (web.xml)
			smtpServer.setRequireTLS(false);
			smtpServer.setHideTLS(false);
			
			smtpServer.setSoftwareName(config.getSoftwareName());
			smtpServer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
