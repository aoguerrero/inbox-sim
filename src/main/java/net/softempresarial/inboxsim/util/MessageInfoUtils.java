package net.softempresarial.inboxsim.util;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class MessageInfoUtils {
	
	private String subject;
	private String from;
	private String to;
	private String date;
	
	public MessageInfoUtils(String fileName) {
		try {
			String messageFile = FileUtils.readFileToString(new File(fileName), "ISO-8859-1");
			Pattern pattern = Pattern.compile("<b>Asunto:</b>(.*)<br/>.*<b>De:</b>(.*)<br/>.*<b>Para:</b>(.*)<br/>.*<b>Fecha:</b>(.*)<br/><div>", Pattern.DOTALL);
			Matcher matcher = pattern.matcher(messageFile);
			while(matcher.find()) {
				subject = matcher.group(1) != null ? matcher.group(1) : "";
				from = matcher.group(2) != null ? matcher.group(2) : "";
				to = matcher.group(3) != null ? matcher.group(3) : "";
				date = matcher.group(4) != null ? matcher.group(4) : "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
