package net.softempresarial.inboxsim.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.softempresarial.inboxsim.util.ConfigReader;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class DownloadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			ConfigReader config = new ConfigReader(getServletContext());
			File file = new File(config.getOutputDir()+config.getFileSeparator()+request.getParameter("file"));
			InputStream inputStream = new FileInputStream(file);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename="+request.getParameter("name"));
			int read = 0;
			byte[] bytes = new byte[1024];
			OutputStream outputStream = response.getOutputStream();

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}