package net.softempresarial.inboxsim.mail;

import org.subethamail.smtp.MessageContext;
import org.subethamail.smtp.MessageHandler;
import org.subethamail.smtp.MessageHandlerFactory;

import net.softempresarial.inboxsim.util.ConfigReader;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class InboxSimMessageHandlerFactory implements MessageHandlerFactory {
	
	private ConfigReader config;

	@Override
	public MessageHandler create(MessageContext messageContext) {
		InboxSimMessageHandler qaMailMessageHandler = new InboxSimMessageHandler();
		qaMailMessageHandler.setConfig(config);
		return qaMailMessageHandler;
	}

	public void setConfig(ConfigReader config) {
		this.config = config;
	}
}


