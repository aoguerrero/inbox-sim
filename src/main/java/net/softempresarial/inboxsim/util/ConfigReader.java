package net.softempresarial.inboxsim.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class ConfigReader implements Serializable {

	private static final long serialVersionUID = 1L;

	private String outputDir;
	private boolean enableAuth;
	private int port;
	private boolean enableTLS;
	private String softwareName;
	private String fileSeparator;
	private int messagesPerPage;

	public ConfigReader(ServletContext servletContext) throws Exception {
		this.outputDir = servletContext.getInitParameter("outputDir");
		this.enableAuth = Boolean.valueOf(servletContext.getInitParameter("enableAuth"));
		this.port = Integer.valueOf(servletContext.getInitParameter("port"));
		this.enableTLS = Boolean.valueOf(servletContext.getInitParameter("enableTLS"));
		this.softwareName = servletContext.getInitParameter("softwareName");
		this.fileSeparator = System.getProperty("file.separator");
		this.messagesPerPage = Integer.valueOf(servletContext.getInitParameter("messagesPerPage"));
	}

	public String getOutputDir() {
		return outputDir;
	}

	public boolean isEnableAuth() {
		return enableAuth;
	}

	public int getPort() {
		return port;
	}

	public boolean isEnableTLS() {
		return enableTLS;
	}

	public String getSoftwareName() {
		return softwareName;
	}

	public String getFileSeparator() {
		return fileSeparator;
	}

	public int getMessagesPerPage() {
		return messagesPerPage;
	}

	public void setMessagesPerPage(int messagesPerPage) {
		this.messagesPerPage = messagesPerPage;
	}

}
