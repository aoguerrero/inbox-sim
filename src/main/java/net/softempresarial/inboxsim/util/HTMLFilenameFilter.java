package net.softempresarial.inboxsim.util;

import java.io.File;
import java.io.FilenameFilter;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class HTMLFilenameFilter implements FilenameFilter {

	@Override
	public boolean accept(File file, String name) {
		name = name.toUpperCase();
		return name.contains("HTML");
	}
}
