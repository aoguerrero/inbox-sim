package net.softempresarial.inboxsim.mail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.subethamail.smtp.MessageHandler;
import org.subethamail.smtp.RejectException;
import org.subethamail.smtp.TooMuchDataException;

import net.softempresarial.inboxsim.util.ConfigReader;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class InboxSimMessageHandler implements MessageHandler {

	private ConfigReader config;

	public InboxSimMessageHandler() {
	}

	@Override
	public void data(InputStream inputStream) throws RejectException, TooMuchDataException, IOException {
		try {
			String messageID = UUID.randomUUID().toString();
			Session session = Session.getDefaultInstance(System.getProperties());
			MimeMessage message = new MimeMessage(session, inputStream);

			StringBuffer fullMessage = new StringBuffer();

			String subject = MimeUtility.decodeText(message.getSubject());
			fullMessage
					.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
			fullMessage.append("<html>\n<head>\n");
			fullMessage.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">");
			fullMessage.append("<meta http-equiv=\"Cache-Control\" content=\"no-cache, no-store, must-revalidate\" />");
			fullMessage.append("<meta http-equiv=\"Pragma\" content=\"no-cache\" />");
			fullMessage.append("<meta http-equiv=\"Expires\" content=\"0\" />");
			fullMessage.append("<title>" + subject + "</title>\n</head>\n");
			fullMessage.append("<body style=\"font-family: sans-serif; margin-top: 40px; margin-left: 40px; margin-right:40px;\">\n");

			fullMessage.append("<b>Asunto:</b>" + subject + "<br/>\n");

			StringBuffer senders = new StringBuffer();
			for (Address sender : message.getFrom()) {
				senders.append(StringEscapeUtils.escapeHtml4(MimeUtility.decodeText(sender.toString())));
				senders.append(" ");
			}
			fullMessage.append("<b>De:</b> " + senders.toString() + "<br/>\n");

			StringBuffer recipients = new StringBuffer();
			for (Address recipient : message.getAllRecipients()) {
				recipients.append(StringEscapeUtils.escapeHtml4(MimeUtility.decodeText(recipient.toString())));
				recipients.append(" ");
			}
			fullMessage.append("<b>Para:</b>" + recipients.toString() + "<br/>\n");

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
			String sentDate = "";
			if (message.getSentDate() != null) {
				sentDate = sdf.format(message.getSentDate());
			} else if (message.getReceivedDate() != null) {
				sentDate = sdf.format(message.getReceivedDate());
			} else {
				sentDate = sdf.format(new Date());
			}
			fullMessage.append("<b>Fecha:</b>" + sentDate + "<br/><div>\n");
			fullMessage.append("<br/>");
			fullMessage.append("<button onclick=\"window.history.back()\">Regresar</button>");
			fullMessage.append("<br/>");
			fullMessage.append("<br/>");
			
			fullMessage.append("<div style=\"width: 800px; height: 400px; overflow: scroll; border-style: solid; border-width: 1px; padding: 5px;\">");
			String messageBody = getBody(messageID, message);
			fullMessage.append(messageBody);
			fullMessage.append("</div>");

			List<String[]> files = getAttachments(messageID, message);
			if (files.size() > 0) {
				fullMessage.append("<br/>\n");
				fullMessage.append("<div>\n");
				fullMessage.append("<b>Adjuntos:</b><br/>\n");
				fullMessage.append("<ul>\n");
				for (String[] file : files) {
					String queryString = "download?file=" + messageID + config.getFileSeparator() + file[1] + "&name=" + URLEncoder.encode(file[0], "ISO-8859-1");
					fullMessage.append("<li><a href=\""+queryString+"\">"+file[0]+"</a></li>\n");
				}
				fullMessage.append("</ul>\n");
				fullMessage.append("</div>\n");				
			}
			fullMessage.append("</div>\n");
			fullMessage.append("</body>\n</html>\n");			
			FileUtils.writeStringToFile(new File(config.getOutputDir() + config.getFileSeparator() + messageID
					+ ".html"), fullMessage.toString(), "ISO-8859-1");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void done() {

	}

	@Override
	public void from(String from) throws RejectException {
	}

	@Override
	public void recipient(String arg0) throws RejectException {

	}

	public void setConfig(ConfigReader config) {
		this.config = config;
	}

	/**
	 * Retorna los adjuntos de un mensaje.
	 * 
	 * @param messageId
	 * @param message
	 * @throws Exception
	 */
	private List<String[]> getAttachments(String messageID, Message message) throws Exception {

		List<String[]> fileNames = new ArrayList<String[]>();

		String attachmentsDir = config.getOutputDir() + config.getFileSeparator() + messageID;
		File fAttachmentsDir = new File(attachmentsDir);
		fAttachmentsDir.mkdir();

		if (message.getContent() instanceof Multipart) {

			Multipart multipart = (Multipart) message.getContent();

			for (int i = 0; i < multipart.getCount(); i++) {
				BodyPart bodyPart = multipart.getBodyPart(i);

				if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
					continue;
				}

				String messageFileName = "file-attachment";
				String internalName = UUID.randomUUID().toString();
				if (bodyPart.getFileName() != null && bodyPart.getFileName().trim().length() > 0) {
					messageFileName = MimeUtility.decodeText(bodyPart.getFileName());
				}

				String fileName = attachmentsDir + config.getFileSeparator() + internalName;
				InputStream is = bodyPart.getInputStream();
				File f = new File(fileName);
				FileOutputStream fos = new FileOutputStream(f);
				byte[] buf = new byte[4096];
				int bytesRead;
				while ((bytesRead = is.read(buf)) != -1) {
					fos.write(buf, 0, bytesRead);
				}
				fos.close();

				fileNames.add(new String[]{messageFileName, internalName});
			}
		}
		return fileNames;
	}

	/**
	 * Retorna el texto del mensaje.
	 * 
	 * @param messageID
	 * @param message
	 * @return
	 * @throws Exception
	 */
	private String getBody(String messageID, Message message) throws Exception {

		StringBuffer text = new StringBuffer();

		String attachmentsDir = config.getOutputDir() + config.getFileSeparator() + messageID;
		File fAttachmentsDir = new File(attachmentsDir);
		fAttachmentsDir.mkdir();

		if (message.getContent() instanceof Multipart) {
			Multipart multipart = (Multipart) message.getContent();
			multipart.getContentType();
			for (int i = 0; i < multipart.getCount(); i++) {
				BodyPart bodyPart = multipart.getBodyPart(i);
				if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
					StringWriter writer = new StringWriter();
					IOUtils.copy(bodyPart.getInputStream(), writer, "ISO-8859-1");
					text.append(writer.toString());
				}
			}
		} else {
			text.append(MimeUtility.decodeText(message.getContent().toString()));
		}
		return text.toString();
	}

}
