<%@page import="net.softempresarial.inboxsim.util.MessageInfoUtils"%>
<%@page
	import="org.apache.commons.io.comparator.LastModifiedFileComparator"%>
<%@page import="java.util.Arrays"%>
<%@page import="net.softempresarial.inboxsim.util.HTMLFilenameFilter"%>
<%@page import="java.io.File"%>
<%@page import="net.softempresarial.inboxsim.util.ConfigReader"%>
<%@page language="java" contentType="text/html"	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	ConfigReader configReader = new ConfigReader(getServletContext());
	String outputDir = configReader.getOutputDir();
	File fOutputDir = new File(outputDir);
	File[] allFiles = fOutputDir.listFiles(new HTMLFilenameFilter());
	Arrays.sort(allFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);

	String pageNumberStr = request.getParameter("page");
	int pageNumber;
	try {
		pageNumber = Integer.valueOf(pageNumberStr);
		if (pageNumber < 0) {
			pageNumber = 0;
		}
	} catch (Exception e) {
		pageNumber = 0;
	}

	int recordsPerPage = configReader.getMessagesPerPage();
	
	boolean enableNext = true;
	int start = pageNumber * recordsPerPage;
	int end = (pageNumber * recordsPerPage) + recordsPerPage;
	if (end >= allFiles.length) {
		enableNext = false;
		end = allFiles.length;
	}
	
	boolean enablePrevious = true;
	if(start == 0) {
		enablePrevious = false;
	}

	File[] files = Arrays.copyOfRange(allFiles, start, end);
%>
<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title><%=getServletContext().getInitParameter("softwareName")%></title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<table>
	<tr>
	<td>
	<img src="ico.png" />
	</td>
	<td>
	<h1><%=getServletContext().getInitParameter("softwareName")%></h1>
	</td>
	</table>
	<a href="index.jsp?page=0">Ir al inicio</a>
	<br />
	<br />
	<table class="paginationTable">
		<tr>
			<td style="width: 33%">
			<% if(enablePrevious) { %>
				<a href="index.jsp?page=<%=pageNumber - 1%>">&lt; Anterior</a>
			<%} else {%>
				&nbsp;
			<%}%>
			</td>
			<td style="width: 33%; text-align: center;"><a
				href="index.jsp?page=<%=pageNumber%>">Recargar</a></td>
			<td style="width: 33%; text-align: right;">
			<%if (enableNext) {%>
				<a href="index.jsp?page=<%=pageNumber + 1%>">Siguiente &gt;</a>
			<%} else {%>
				&nbsp;
			<%}%>
			</td>
		</tr>
	</table>

	<table class="messagesTable">
		<tr>
			<th class="tableHeader" style="width: 40%">Asunto</th>
			<th class="tableHeader" style="width: 15%">Fecha</th>
			<th class="tableHeader" style="width: 20%">De</th>
			<th class="tableHeader" style="width: 20%">Para</th>
			<th class="tableHeader" style="width: 5%">&nbsp;</th>
		</tr>
		<%
			for (File file : files) {
				MessageInfoUtils info = new MessageInfoUtils(file.getPath());
		%>
		<tr>
			<td class="tableCell"><%=info.getSubject()%></td>
			<td class="tableCell"><%=info.getDate()%></td>
			<td class="tableCell"><%=info.getFrom()%></td>
			<td class="tableCell"><%=info.getTo()%></td>
			<td class="tableCell" style="text-align: center;"><a
				href="open?message=<%=file.getName()%>">Ver</a>
			</td>
		</tr>
		<%
			}
		%>
	</table>

	<table class="paginationTable">
		<tr>
			<td style="width: 33%">
			<% if(enablePrevious) { %>
				<a href="index.jsp?page=<%=pageNumber - 1%>">&lt; Anterior</a>
			<%} else {%>
				&nbsp;
			<%}%>
			</td>
			<td style="width: 33%; text-align: center;"><a
				href="index.jsp?page=<%=pageNumber%>">Recargar</a></td>
			<td style="width: 33%; text-align: right;">
			<%if (enableNext) {%>
				<a href="index.jsp?page=<%=pageNumber + 1%>">Siguiente &gt;</a>
			<%} else {%>
				&nbsp;
			<%}%>
			</td>
		</tr>
	</table>

</body>
</html>