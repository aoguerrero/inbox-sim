package net.softempresarial.inboxsim.mail;

import java.util.Arrays;
import java.util.List;

import org.subethamail.smtp.AuthenticationHandler;
import org.subethamail.smtp.AuthenticationHandlerFactory;

import net.softempresarial.inboxsim.util.ConfigReader;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class InboxSimAuthHandlerFactory implements AuthenticationHandlerFactory {

	private ConfigReader config;

	@Override
	public AuthenticationHandler create() {
		return new InboxSimAuthHandler();
	}

	@Override
	public List<String> getAuthenticationMechanisms() {
		return Arrays.asList(new String[]{"PLAIN", "LOGIN"});
	}

	public void setConfig(ConfigReader config) {
		this.config = config;
	}
}
