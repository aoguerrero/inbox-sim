package net.softempresarial.inboxsim.mail;

import org.subethamail.smtp.AuthenticationHandler;
import org.subethamail.smtp.RejectException;

/**
 * @author Andrés Guerrero <aoguerrero@gmail.com>
 */
public class InboxSimAuthHandler implements AuthenticationHandler {

	@Override
	public String auth(String arg0) throws RejectException {
		return null;
	}

	@Override
	public Object getIdentity() {
		return "user";
	}

}
